﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            actualizarEstadoListas();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "iniciarSesion";
            comando.Parameters.AddWithValue("@nombre", tUsuario.Text);
            comando.Parameters.AddWithValue("@pass", tPass.Text);
            
            var resultado = new SqlParameter
            {
                ParameterName = "@resultado",
                Direction = ParameterDirection.Output,
                Size = 5
            };
            comando.Parameters.Add(resultado);

            var tipoEmpleado = new SqlParameter
            {
                ParameterName = "@tipo",
                Direction = ParameterDirection.Output,
                Size = 5
            };
            comando.Parameters.Add(tipoEmpleado);
            comando.ExecuteNonQuery();
            int salida = Convert.ToInt32(comando.Parameters["@resultado"].Value);
            int tipo = Convert.ToInt32(comando.Parameters["@tipo"].Value);
            conexion.Close();
            
            if (salida == 1)
            {
                switch (tipo)
                {
                    case 1:
                        Application["nitEmpleado"] = tUsuario.Text;
                        Response.Redirect("vVendedor");
                        break;
                    case 2:
                        Application["nitEmpleado"] = tUsuario.Text;
                        Response.Redirect("vJefes");
                        break;
                    case 3:
                        Application["nitEmpleado"] = tUsuario.Text;
                        Response.Redirect("vJefes");
                        break;
                    case 4:
                        Application["nitEmpleado"] = tUsuario.Text;
                        Response.Redirect("vAdmin");
                        break;
                    default:
                        break;
                }
            }
            else if (salida == 0)
            {
                tUsuario.Text = Convert.ToString(salida);

            }

        }

        protected void bPagar_Click(object sender, EventArgs e)
        {
            Response.Redirect("pagar");
        }

        protected void actualizarEstadoListas()
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                string query = "exec actualizarEstadoListas";
                SqlCommand comando = new SqlCommand(query, conexion);
                conexion.Open();
                comando.ExecuteNonQuery();
                conexion.Close();
            }
        }
    }
}