﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="crearOrden.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.crearOrden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h2>Crear Orden</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Cliente:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dCliente" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="bCargarCliente" runat="server" OnClick="bCargarCliente_Click" Text="Cargar" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Empleado:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tEmpleado" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="bDatos" runat="server" OnClick="bDatos_Click" Text="Ver datos" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="Listas asignadas:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dListaA" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="bmProducto" runat="server" OnClick="bmProducto_Click" Text="Mostrar Productos" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Listas disponibles:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dLista" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="bAsignar" runat="server" OnClick="bAsignar_Click" Text="Asignar" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Orden:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tOrden" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="bVerLista" runat="server" OnClick="bVerLista_Click" Text="Ver Lista" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Producto:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dProducto" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Cantidad:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCantidad" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="bAgregar" runat="server" Text="Agregar" OnClick="Button1_Click" />
            </td>
            <td>
                <asp:Button ID="bEliminar" runat="server" Text="Eliminar" OnClick="bEliminar_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Total:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tSubtotal" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Crédito:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCredito" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="bCrear" runat="server" Text="Crear" OnClick="bCrear_Click" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" OnClick="bSalir_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
