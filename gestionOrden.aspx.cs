﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class gestionOrden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            //string consulta = ("select Id as No, Cliente.Nombre, Cliente.Disponible, Orden.Total as Total from Orden inner Join Cliente on Orden.Cliente_NIT = Cliente.NIT where Orden.Estado_Orden = 3;");
            string consulta = "select Id as No, Cliente.Nombre, Cliente.Disponible, Orden.Total as Total from Orden inner Join Cliente on Orden.Cliente_NIT = Cliente.NIT " +
                "inner Join Empleado on Empleado.NIT = Orden.Empleado_NIT where Orden.Estado_Orden = 3 and Orden.Cliente_NIT is not null " +
                "and(Empleado.NIT = '" + Application["nitEmpleado"].ToString() + "' or Empleado.Jefe = " + Application["nitEmpleado"].ToString() + ")";
            DataTable tabla = new DataTable();
            using (SqlConnection conexion = new SqlConnection(conexionString))
            {
                conexion.Open();
                SqlDataAdapter adaptador = new SqlDataAdapter(consulta, conexion);
                adaptador.Fill(tabla);
                conexion.Close();
            }
            gOrden.DataSource = tabla;
            gOrden.DataBind();
        }

        protected void bAprobar_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            int decision = Convert.ToInt32(tOrden.Text);
            string query = "exec autorizarOrden"+" "+decision+","+1+";";
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
            tOrden.Text = string.Empty;
            conexion.Close();
            generarPdf(decision);

        }

        protected void bRechazar_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            int decision = Convert.ToInt32(tOrden.Text);
            string query = "exec autorizarOrden " + decision + "," + 2 + ";";
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
            tOrden.Text = string.Empty;
            conexion.Close();
        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("vJefes");
        }

        protected void generarPdf(int orden)
        {
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "reporte1.pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.AddTitle("Orden aprobada");
            doc.Open();
            doc.Add(new Paragraph("Orden Aprobada"));
            doc.Add(new Paragraph("No. Orden: "+orden));

            doc.Close();
            writer.Close();
            fs.Close();

        }
    }
}