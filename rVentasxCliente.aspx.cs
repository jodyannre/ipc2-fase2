﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;
using System.Web.WebSockets;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class reportes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string query = "select * from Cliente";
                string nombre = "NIT";
                string valor = "NIT";
                cargarList(query, nombre, valor, dCliente);
            }
        }

        protected void bGenerar_Click(object sender, EventArgs e)
        {
            generarReporte();

        }
        protected void generarReporte()
        {
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "Reporte-"+ dCliente.SelectedValue.ToString() + ".pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph titulo = new Paragraph();
            DateTime fechaI = Convert.ToDateTime(tFI.Text);
            string dateI = fechaI.ToString("dd/MM/yyyy");
            DateTime fechaF = Convert.ToDateTime(tFF.Text);
            string dateF = fechaF.ToString("dd/MM/yyyy");
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);


            string query1 = "Select NIT, Nombre, Direccion from Cliente where NIT = " + dCliente.SelectedValue.ToString();
            string query2 = "Select NIT, Empleado.Nombre, Puesto.Nombre as Puesto from Empleado inner join Puesto " +
                "on Puesto.Id_Puesto = Empleado.Puesto inner join Orden on Orden.Empleado_NIT = Empleado.NIT where Empleado.NIT in " +
                "(select Empleado_NIT from Orden where Cliente_NIT = '" + dCliente.SelectedValue.ToString() + "') and " +
                "Orden.Fecha_Limite <= '" + dateF + "' and Orden.Fecha_Limite >= '" + dateI + "' group by NIT, Empleado.Nombre, Puesto.Nombre";


            string query3 = "select Producto.Nombre as Producto, Catalogo_Producto_Precio.Precio, sum(Productos_Orden.Cantidad) as Cantidad, " +
                "sum(Productos_Orden.Total) as total from Producto inner join Catalogo_Producto_Precio on Producto.Codigo = " +
                "Catalogo_Producto_Precio.Producto inner join Productos_Orden on Productos_Orden.Producto = Catalogo_Producto_Precio.Id inner " +
                "join Orden on Orden.Id = Productos_Orden.Orden where Orden.Cliente_NIT = '" + dCliente.SelectedValue.ToString() + "' " +
                "and Orden.Fecha_Limite <= '" + dateF + "' and " +
                "Orden.Fecha_Limite >= '" + dateI + "' group by Producto.Nombre, Catalogo_Producto_Precio.Precio";



            string query4 = "select sum(Total) as Sumatoria from Orden where Orden.Cliente_NIT = " + dCliente.SelectedValue.ToString() + " " +
                "and Orden.Fecha_Limite <= '" + dateF + "' and Orden.Fecha_Limite >= '" + dateI + "'";

            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_CENTER;
            DataTable dtCliente = consultasPDF(query1);
            DataTable dEmpleado = consultasPDF(query2);
            DataTable dDetalle = consultasPDF(query3);
            DataTable dTotal = consultasPDF(query4);
            PdfPTable tCliente = new PdfPTable(3);
            PdfPTable tEmpleado = new PdfPTable(3);
            PdfPTable tDetalle = new PdfPTable(4);
            PdfPTable tTotal = new PdfPTable(1);

            tCliente.AddCell("NIT");
            tCliente.AddCell("Nombre");
            tCliente.AddCell("Dirección");

            foreach (DataRow fila in dtCliente.Rows)
            {
                tCliente.AddCell(fila["NIT"].ToString());
                tCliente.AddCell(fila["Nombre"].ToString());
                tCliente.AddCell(fila["Direccion"].ToString());
            }

            tEmpleado.AddCell("NIT");
            tEmpleado.AddCell("Nombre");
            tEmpleado.AddCell("Puesto");

            foreach (DataRow fila in dEmpleado.Rows)
            {
                tEmpleado.AddCell(fila["NIT"].ToString());
                tEmpleado.AddCell(fila["Nombre"].ToString());
                tEmpleado.AddCell(fila["Puesto"].ToString());
            }

            tDetalle.AddCell("Producto");
            tDetalle.AddCell("Precio");
            tDetalle.AddCell("Cantidad");
            tDetalle.AddCell("Total");

            foreach (DataRow fila in dDetalle.Rows)
            {
                tDetalle.AddCell(fila["Producto"].ToString());
                tDetalle.AddCell(fila["Precio"].ToString());
                tDetalle.AddCell(fila["Cantidad"].ToString());
                tDetalle.AddCell(fila["Total"].ToString());
            }

            tTotal.AddCell("Sumatoria del total");
            foreach (DataRow fila in dTotal.Rows)
            {
                tTotal.AddCell(fila["Sumatoria"].ToString());
            }
            parrafo.Add("Ventas por cliente");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Cliente");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tCliente);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Empleado");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tEmpleado);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Detalle de venta");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tDetalle);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Sumatoria");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tTotal);
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);

            parrafo.Add("Detalle Pago");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);

            query1 = "select convert(Date,Fecha_Pago,105) as Fecha, Orden, Moneda.Nombre as Moneda, Moneda.Cambio as Tasa, cast((Cantidad*Moneda.Cambio) as decimal(18,2)) as Cantidad " +
                "from Pago inner join Moneda on Moneda.Id = Pago.Moneda inner join Orden on Orden.Id = Pago.Orden " +
                "where Orden.Cliente_NIT = '"+ dCliente.SelectedValue.ToString() + "' and Orden.Fecha_Limite <= '"+dateF+"' and Orden.Fecha_Limite >= '"+dateI+"'";
            query2 = "select cast(sum(Cantidad*Moneda.Cambio) as decimal(18,2)) as Sumatoria from Pago inner join Moneda on Moneda.Id = Pago.Moneda " +
                "inner join Orden on Orden.Id = Pago.Orden where Orden.Cliente_NIT = '" + dCliente.SelectedValue.ToString() + "' and Orden.Fecha_Limite <= '" + dateF+"' and " +
                "Orden.Fecha_Limite >= '"+dateI+"'";

            DataTable dDetalle1 = consultasPDF(query1);
            DataTable dDetalle2 = consultasPDF(query2);
            PdfPTable tDetalle1 = new PdfPTable(5);
            PdfPTable tDetalle2 = new PdfPTable(1);

            tDetalle1.AddCell("Fecha");
            tDetalle1.AddCell("Orden");
            tDetalle1.AddCell("Moneda");
            tDetalle1.AddCell("Tasa");
            tDetalle1.AddCell("Monto en $");

            foreach (DataRow fila in dDetalle1.Rows)
            {
                tDetalle1.AddCell(fila["Fecha"].ToString());
                tDetalle1.AddCell(fila["Orden"].ToString());
                tDetalle1.AddCell(fila["Moneda"].ToString());
                tDetalle1.AddCell(fila["Tasa"].ToString());
                tDetalle1.AddCell(fila["Cantidad"].ToString());
            }

            tDetalle2.AddCell("Sumatoria de monto");

            foreach (DataRow fila in dDetalle2.Rows)
            {
                tDetalle2.AddCell(fila["Sumatoria"].ToString());

            }

            doc.Add(tDetalle1);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tDetalle2);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Detalle de notas de crédito por anulación");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            query1 = "select Convert(date,Anulacion.Fecha,125) as Fecha, Orden.Id as Orden, Orden.Abonado as Monto from Orden inner join Anulacion " +
                "on Orden.Anulacion = Anulacion.Id where Anulacion.Fecha <= '"+dateF+"' and Anulacion.Fecha >= '"+dateI+"' and " +
                "Cliente_NIT = '" + dCliente.SelectedValue.ToString() + "'";
            query2 = "select sum(Monto) as Monto from ( select Orden.Abonado as Monto from Orden inner join Anulacion " +
                "on Orden.Anulacion = Anulacion.Id where Anulacion.Fecha <= '" + dateF + "' and Anulacion.Fecha >= '" + dateI + "' and " +
                "Cliente_NIT = '" + dCliente.SelectedValue.ToString() + "' ) as suma";
            DataTable dtAnulacion = consultasPDF(query1);
            DataTable dtMonto = consultasPDF(query2);
            PdfPTable tAnulacion = new PdfPTable(3);
            PdfPTable tMonto = new PdfPTable(1);

            tAnulacion.AddCell("Fecha");
            tAnulacion.AddCell("Código Orden");
            tAnulacion.AddCell("Monto en $");
            tMonto.AddCell("Sumatoria de monto");

            foreach(DataRow fila in dtAnulacion.Rows)
            {
                tAnulacion.AddCell(fila["Fecha"].ToString());
                tAnulacion.AddCell(fila["Orden"].ToString());
                tAnulacion.AddCell(fila["Monto"].ToString());
            }

            foreach(DataRow fila in dtMonto.Rows)
            {
                tMonto.AddCell(fila["Monto"].ToString());
            }

            doc.Add(tAnulacion);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tMonto);
            doc.Close();

        }

        protected DataTable consultasPDF(string query)
        {
            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                adapter.Fill(tabla);
                return tabla;
            }

        }
        protected void cargarList(String query, string nombre, string valor, DropDownList lista)
        {

            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;


            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                SqlCommand comando = new SqlCommand(query, conexion);
                conexion.Open();
                lista.DataSource = comando.ExecuteReader();
                lista.DataTextField = nombre;
                lista.DataValueField = valor;
                lista.DataBind();
                conexion.Close();
            }
        }

        protected void dCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("vJefes.aspx");
        }
    }
}