﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="vJefes.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.vJefes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bOrden" runat="server" Text="Gestion de orden" OnClick="bOrden_Click" />
            </td>
            <td>
                <asp:Button ID="bVenta" runat="server" Text="Ventas" OnClick="bVenta_Click" />
            </td>
            <td>
                <asp:Button ID="bAnulacion" runat="server" OnClick="bAnulacion_Click" Text="Anulaciones" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bReporte1" runat="server" Text="Ventas x Cliente" OnClick="bReporte1_Click" />
            </td>
            <td>
                <asp:Button ID="bReporte2" runat="server" Text="Ventas x producto" OnClick="bReporte2_Click" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" OnClick="bSalir_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
