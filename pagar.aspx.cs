﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text.pdf;
using iTextSharp;
using iTextSharp.text;
using System.Configuration;
using System.Globalization;
using System.IO;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class pagar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "select * from Moneda order by Moneda.Nombre";
            string nombre = "Nombre";
            string valor = "Id";
            cargarList(query, nombre, valor, dMoneda);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string cliente = tCliente.Text;
            int orden = Convert.ToInt32(tOrden.Text);
            decimal cantidad = Convert.ToDecimal(tCantidad.Text);
            int forma = Convert.ToInt32(tForma.Text);
            int moneda = Convert.ToInt32(dMoneda.SelectedValue);
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "pagarOrden";
            comando.Parameters.AddWithValue("@orden", Convert.ToInt32(tOrden.Text));
            comando.Parameters.AddWithValue("@dinero", Convert.ToDecimal(tCantidad.Text));
            comando.Parameters.AddWithValue("@forma", Convert.ToInt32(tForma.Text));
            comando.Parameters.AddWithValue("@autorizacion", 1);
            comando.Parameters.AddWithValue("@cheque", 1);
            comando.Parameters.AddWithValue("@moneda", moneda);

            var resultado = new SqlParameter
            {
                ParameterName = "@salida",
                Direction = ParameterDirection.Output,
                Size = 10
            };
            comando.Parameters.Add(resultado);

            int salida = Convert.ToInt32(comando.Parameters["@salida"].Value);

            comando.ExecuteNonQuery();
            conexion.Close();
            string mensaje = "";
            int validar = CrearFactura();
            switch (validar)
            {
                case 0:
                    mensaje = "El dinero ingresado sobrepasa el saldo pendiente.";
                    break;
                case 1:
                    mensaje = "Orden cancelada";
                    break;
                case 2:
                    mensaje = "Abono registrado";
                    break;
            }
            Response.Write("<script languaje=javascript> alert('" + mensaje + "');</script>");


        }

        protected void bCargar_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "estadoOrden";
            comando.Parameters.AddWithValue("@orden", Convert.ToInt32(tOrden.Text));

            var resultado = new SqlParameter
            {
                ParameterName = "@resultado",
                Direction = ParameterDirection.Output,
                Size = 50000
            };
            comando.Parameters.Add(resultado);

            var restante = new SqlParameter
            {
                ParameterName = "@restante",
                Direction = ParameterDirection.Output,
                Size = 30000
            };
            restante.DbType = DbType.Decimal;
            restante.Precision = 18;
            restante.Scale = 2;
            comando.Parameters.Add(restante);

            var cliente = new SqlParameter
            {
                ParameterName = "@cliente",
                Direction = ParameterDirection.Output,
                Size = 100
            };
            comando.Parameters.Add(cliente);


            comando.ExecuteNonQuery();
            int salida = Convert.ToInt32(comando.Parameters["@resultado"].Value);
            string salida3 = "10";
            decimal salida2 = 0;
            try
            {
                salida2 = Convert.ToDecimal(comando.Parameters["@restante"].Value);
                salida3 = comando.Parameters["@cliente"].Value.ToString();
            }catch(Exception ex)
            {

            }

            conexion.Close();
            tCliente.Text = salida3;


            switch (salida)
            {
                case 1:
                    lResultado.Text = "Faltan: " + salida2.ToString("C", new CultureInfo("en-US"));
                    break;
                case 2:
                    lResultado.Text = "La orden fue rechazada.";
                    break;
                case 3:
                    lResultado.Text = "La orden aún no ha sido evaluada";
                    break;
                case 4:
                    lResultado.Text = "Orden pagada";
                    break;
                case 5:
                    lResultado.Text = "Orden Retrazsada";
                    break;
                case 6:
                    lResultado.Text = "Orden anulada";
                    break;
                case 7:
                    lResultado.Text = "Orden inexistente";
                    break;
                default:
                    break;
            }


        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("login");
        }

        protected void cargarList(String query, string nombre, string valor, DropDownList lista)
        {

            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            if (!this.IsPostBack)
            {
                using (SqlConnection conexion = new SqlConnection(conexionSTR))
                {
                    SqlCommand comando = new SqlCommand(query, conexion);
                    conexion.Open();
                    lista.DataSource = comando.ExecuteReader();
                    lista.DataTextField = nombre;
                    lista.DataValueField = valor;
                    lista.DataBind();
                    conexion.Close();
                }
            }



        }

        protected void bCalcular_Click(object sender, EventArgs e)
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            SqlConnection conexion = new SqlConnection(conexionSTR);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "calcularPrecio";
            comando.Parameters.AddWithValue("@dinero", Convert.ToDecimal(tCantidad.Text));
            comando.Parameters.AddWithValue("@moneda", Convert.ToInt32(dMoneda.SelectedValue));

            var resultado = new SqlParameter
            {
                ParameterName = "@valorReal",
                Direction = ParameterDirection.Output,
                Size = 50000
            };
            resultado.DbType = DbType.Decimal;
            resultado.Precision = 18;
            resultado.Scale = 2;
            comando.Parameters.Add(resultado);

            comando.ExecuteNonQuery();
            decimal salida = Convert.ToDecimal(comando.Parameters["@valorReal"].Value);
            lCalcular.Text = "Equivalen a: "+salida.ToString("C", new CultureInfo("en-US"));
            conexion.Close();
        }

        protected int CrearFactura()
        {
            int idFactura = validarFactura();

            if (idFactura == 0)
            {
                return 2;
            }
            else
            {
                System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "factura-" + idFactura + ".pdf", FileMode.Create);
                Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.Open();
                Paragraph parrafo = new Paragraph();
                parrafo.Alignment = Element.ALIGN_CENTER;

                string query1 = "select Id_Factura as Factura, Serie, Orden.Cliente_NIT as Cliente, Orden.Empleado_NIT as Vendedor, " +
                    "Convert(varchar, Fecha, 6) as Fecha, Orden, Total from Factura inner join Orden on Orden.Id = Factura.Orden where " +
                    "Factura.Id_Factura = "+idFactura;
                string query2 = "select Producto.Nombre as Producto, Productos_Orden.Cantidad, Productos_Orden.Total from Productos_Orden " +
                    "inner join Catalogo_Producto_Precio on Productos_Orden.Producto = Catalogo_Producto_Precio.Id " +
                    "inner join Producto on Catalogo_Producto_Precio.Producto = Producto.Codigo " +
                    "inner join Orden on Orden.Id = Productos_Orden.Orden where Orden = "+Convert.ToInt32(tOrden.Text);

                DataTable dFactura = consultasPDF(query1);
                DataTable dDetalle = consultasPDF(query2);
                PdfPTable tFactura = new PdfPTable(7);
                PdfPTable tDetalle = new PdfPTable(3);

                tFactura.AddCell("Factura");
                tFactura.AddCell("Serie");
                tFactura.AddCell("Cliente");
                tFactura.AddCell("Vendedor");
                tFactura.AddCell("Fecha");
                tFactura.AddCell("Orden");
                tFactura.AddCell("Total");
                tDetalle.AddCell("Producto");
                tDetalle.AddCell("Cantidad");
                tDetalle.AddCell("Total");
                foreach (DataRow fila in dFactura.Rows)
                {
                    tFactura.AddCell(fila["Factura"].ToString());
                    tFactura.AddCell(fila["Serie"].ToString());
                    tFactura.AddCell(fila["Cliente"].ToString());
                    tFactura.AddCell(fila["Vendedor"].ToString());
                    tFactura.AddCell(fila["Fecha"].ToString());
                    tFactura.AddCell(fila["Orden"].ToString());
                    tFactura.AddCell(fila["Total"].ToString());
                }
                foreach(DataRow fila in dDetalle.Rows)
                {
                    tDetalle.AddCell(fila["Producto"].ToString());
                    tDetalle.AddCell(fila["Cantidad"].ToString());
                    tDetalle.AddCell(fila["Total"].ToString());
                }
                parrafo.Add("Factura");
                doc.Add(parrafo);
                parrafo.Clear();
                doc.Add(Chunk.NEWLINE);
                doc.Add(tFactura);
                parrafo.Add("Detalle de factura");
                doc.Add(parrafo);
                parrafo.Clear();
                doc.Add(Chunk.NEWLINE);
                doc.Add(tDetalle);
                doc.Close();
                return 1;
            }
        }
        protected int validarFactura()
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            DateTime date = DateTime.Today;
            string fecha = date.Date.ToString("dd/MM/yyyy");
            string query = "select Id_Factura from Factura where Orden = " + Convert.ToInt32(tOrden.Text);
            int idFactura = 0;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))

            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                try
                {
                    idFactura = Convert.ToInt32(comando.ExecuteScalar());
                }catch(Exception ex)
                {
                    idFactura = 0;
                }


            }
            return idFactura;

        }
        protected DataTable consultasPDF(string query)
        {
            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                adapter.Fill(tabla);
                return tabla;
            }

        }
    }
}
