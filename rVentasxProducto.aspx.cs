﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class rVentasxProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string producto = "select * from Producto";
                string nombre = "Nombre";
                string valor = "Codigo";
                string categoria = "select * from Categoria_Producto";
                string valor2 = "Id";
                cargarList(producto, nombre, valor, dProducto);
                cargarList(categoria, nombre, valor2, dCategoria);
            }
        }

        protected void cargarList(String query, string nombre, string valor, DropDownList lista)
        {

            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;


            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                SqlCommand comando = new SqlCommand(query, conexion);
                conexion.Open();
                lista.DataSource = comando.ExecuteReader();
                lista.DataTextField = nombre;
                lista.DataValueField = valor;
                lista.DataBind();
                conexion.Close();
            }
        }

        protected void bSalir1_Click(object sender, EventArgs e)
        {
            Response.Redirect("vJefes.aspx");
        }

        protected void bSalir2_Click(object sender, EventArgs e)
        {
            Response.Redirect("vJefes.aspx");
        }
        protected DataTable consultasPDF(string query)
        {
            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                adapter.Fill(tabla);
                return tabla;
            }

        }
        protected void generarReporteProducto()
        {
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "Reporte2-Producto.pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph titulo = new Paragraph();
            DateTime fechaI = Convert.ToDateTime(tFI.Text);
            string dateI = fechaI.ToString("dd/MM/yyyy");
            DateTime fechaF = Convert.ToDateTime(tFF.Text);
            string dateF = fechaF.ToString("dd/MM/yyyy");
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);


            string query1 = "select Codigo, Descripcion from Producto where Codigo = " + Convert.ToInt32(dProducto.SelectedValue);
            string query2 = "select convert(date,Orden.Fecha_Limite,105) as Fecha, Orden.Id as Orden, Productos_Orden.Cantidad, Catalogo_Producto_Precio.Precio, Productos_Orden.Total, " +
                "Catalogo_Producto_Precio.Producto from Orden inner join Productos_Orden on Orden.Id = Productos_Orden.Orden " +
                "inner join Catalogo_Producto_Precio on Catalogo_Producto_Precio.Id = Productos_Orden.Producto " +
                "where Catalogo_Producto_Precio.Producto = " + Convert.ToInt32(dProducto.SelectedValue) + " and Orden.Fecha_Limite is not null " +
                "and Orden.Fecha_Limite <= '" + fechaF + "' and Orden.Fecha_Limite >= '" + fechaI + "'";


            string query3 = "select sum(Productos_Orden.Cantidad) as Cantidad, sum(Productos_Orden.Total) as Total " +
                "from Orden inner join Productos_Orden on Orden.Id = Productos_Orden.Orden inner join Catalogo_Producto_Precio on " +
                "Catalogo_Producto_Precio.Id = Productos_Orden.Producto where Catalogo_Producto_Precio.Producto = " + Convert.ToInt32(dProducto.SelectedValue) + " and " +
                "Orden.Fecha_Limite is not null and Orden.Fecha_Limite <= '" + fechaF + "' and Orden.Fecha_Limite >= '" + fechaI + "'";

            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_CENTER;
            DataTable dtProducto = consultasPDF(query1);
            DataTable dtDatosP = consultasPDF(query2);
            DataTable dSumatoriaP = consultasPDF(query3);
            PdfPTable tProducto = new PdfPTable(2);
            PdfPTable tDatosP = new PdfPTable(5);
            PdfPTable tSumatoriaP = new PdfPTable(2);

            tProducto.AddCell("Codigo");
            tProducto.AddCell("Descripción");
            foreach (DataRow fila in dtProducto.Rows)
            {
                tProducto.AddCell(fila["Codigo"].ToString());
                tProducto.AddCell(fila["Descripcion"].ToString());
            }


            tDatosP.AddCell("Fecha");
            tDatosP.AddCell("Orden");
            tDatosP.AddCell("Cantidad");
            tDatosP.AddCell("Precio");
            tDatosP.AddCell("Total");

            foreach (DataRow fila in dtDatosP.Rows)
            {
                tDatosP.AddCell(fila["Fecha"].ToString());
                tDatosP.AddCell(fila["Orden"].ToString());
                tDatosP.AddCell(fila["Cantidad"].ToString());
                tDatosP.AddCell(fila["Precio"].ToString());
                tDatosP.AddCell(fila["Total"].ToString());
            }

            tSumatoriaP.AddCell("Sumatoria Cantidad");
            tSumatoriaP.AddCell("Sumatoria Total");

            foreach (DataRow fila in dSumatoriaP.Rows)
            {
                tSumatoriaP.AddCell(fila["Cantidad"].ToString());
                tSumatoriaP.AddCell(fila["Total"].ToString());
            }


            parrafo.Add("Ventas por Producto");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tProducto);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tDatosP);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tSumatoriaP);
            doc.Add(Chunk.NEWLINE);
            doc.Close();
        }

        protected void generarReporteCategoria()
        {
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "Reporte2-Categoria.pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph titulo = new Paragraph();
            DateTime fechaI = Convert.ToDateTime(tFI.Text);
            string dateI = fechaI.ToString("dd/MM/yyyy");
            DateTime fechaF = Convert.ToDateTime(tFF.Text);
            string dateF = fechaF.ToString("dd/MM/yyyy");
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);


            string query1 = "select * from Categoria_Producto where Id = " + Convert.ToInt32(dCategoria.SelectedValue);
            string query2 = "select Producto.Nombre, sum(Productos_Orden.Cantidad) as Cantidad, count(Productos_Orden.Orden) as Cantidad_Ordenes, " +
                "cast(avg(Productos_Orden.Cantidad) as decimal (18,2)) as Prom_Cantidad, cast(avg(Productos_Orden.Total) as decimal (18,2)) as PromPorOrden from  " +
                "Productos_Orden inner join Catalogo_Producto_Precio on Productos_Orden.Producto =  " +
                "Catalogo_Producto_Precio.Id inner join Producto on Catalogo_Producto_Precio.Producto = " +
                "Producto.Codigo inner join Categoria_Producto on Producto.Categoria = Categoria_Producto.Id " +
                "inner join Orden on Orden.Id = Productos_Orden.Orden " +
                "where Productos_Orden.Total is not null and orden.Fecha_Limite is not null and " +
                "Orden.Fecha_Limite <= '"+fechaF+"' and Orden.Fecha_Limite >= '"+fechaI+"' and Producto.Categoria = "+ Convert.ToInt32(dCategoria.SelectedValue) + " " +
                "group by Producto.Nombre ";




            string query3 = "select sum(PromPorOrden) as PromPorOrden, sum(Prom_Cantidad) as Prom_Cantidad from " +
                "( " +
                "select cast(avg(Productos_Orden.Cantidad) as decimal (18,2)) as Prom_Cantidad, cast(avg(Productos_Orden.Total) as decimal (18,2)) as PromPorOrden from " +
                "Productos_Orden inner join Catalogo_Producto_Precio on Productos_Orden.Producto = " +
                "Catalogo_Producto_Precio.Id inner join Producto on Catalogo_Producto_Precio.Producto = " +
                "Producto.Codigo inner join Categoria_Producto on Producto.Categoria = Categoria_Producto.Id " +
                "inner join Orden on Orden.Id = Productos_Orden.Orden where Productos_Orden.Total is not null and orden.Fecha_Limite is not null and " +
                "Orden.Fecha_Limite <= '" + fechaF + "' and Orden.Fecha_Limite >= '" + fechaI + "' " +
                "and Producto.Categoria = " + Convert.ToInt32(dCategoria.SelectedValue) + " group by Producto.Nombre ) as resultado";

            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_CENTER;
            DataTable dtProducto = consultasPDF(query1);
            DataTable dtDatosP = consultasPDF(query2);
            DataTable dSumatoriaP = consultasPDF(query3);
            PdfPTable tProducto = new PdfPTable(2);
            PdfPTable tDatosP = new PdfPTable(5);
            PdfPTable tSumatoriaP = new PdfPTable(2);

            tProducto.AddCell("Id");
            tProducto.AddCell("Nombre");
            foreach (DataRow fila in dtProducto.Rows)
            {
                tProducto.AddCell(fila["Id"].ToString());
                tProducto.AddCell(fila["Nombre"].ToString());
            }


            tDatosP.AddCell("Nombre");
            tDatosP.AddCell("Cantidad");
            tDatosP.AddCell("Cantidad Órdenes");
            tDatosP.AddCell("Promedio Cantidad");
            tDatosP.AddCell("Promedio x Orden");

            foreach (DataRow fila in dtDatosP.Rows)
            {
                tDatosP.AddCell(fila["Nombre"].ToString());
                tDatosP.AddCell(fila["Cantidad"].ToString());
                tDatosP.AddCell(fila["Cantidad_Ordenes"].ToString());
                tDatosP.AddCell(fila["Prom_Cantidad"].ToString());
                tDatosP.AddCell(fila["PromPorOrden"].ToString());
            }

            tSumatoriaP.AddCell("Sumatoria de Promedio x Orden");
            tSumatoriaP.AddCell("Sumatoria de Promedio x Cantidad");

            foreach (DataRow fila in dSumatoriaP.Rows)
            {
                tSumatoriaP.AddCell(fila["PromPorOrden"].ToString());
                tSumatoriaP.AddCell(fila["Prom_Cantidad"].ToString());
            }


            parrafo.Add("Ventas por Categoría");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tProducto);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tDatosP);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tSumatoriaP);
            doc.Add(Chunk.NEWLINE);
            doc.Close();
        }

        protected void bCrearC_Click(object sender, EventArgs e)
        {
            generarReporteCategoria();
        }

        protected void bCrearP_Click(object sender, EventArgs e)
        {
            generarReporteProducto();
        }
    }
}