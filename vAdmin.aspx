﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="vAdmin.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.vAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Button ID="bCliente" runat="server" OnClick="Button1_Click" Text="Crear Cliente" />
            </td>
            <td>
                <asp:Button ID="bEmpleado" runat="server" OnClick="Button2_Click" Text="Crear Empleado" />
            </td>
            <td>
                <asp:Button ID="bVehiculo" runat="server" OnClick="Button3_Click" Text="Crear vehículo" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bMeta" runat="server" OnClick="Button4_Click" Text="Crear Meta" />
            </td>
            <td>
                <asp:Button ID="bCategoria" runat="server" OnClick="Button5_Click" Text="Crear Categoría" />
            </td>
            <td>
                <asp:Button ID="bProducto" runat="server" OnClick="Button6_Click" Text="Crear Producto" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bAVehiculo" runat="server" OnClick="Button7_Click" Text="Asignar Vehículo" />
            </td>
            <td>
                <asp:Button ID="bCarga" runat="server" OnClick="Button8_Click" Text="Carga masiva" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" OnClick="bSalir_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
