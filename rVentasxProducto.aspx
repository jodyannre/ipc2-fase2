﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="rVentasxProducto.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.rVentasxProducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    Ventas por Producto y por Categoría
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Venta por Producto"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Código producto:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dProducto" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Código categoría:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dCategoria" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Fecha Inicial:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tFI" runat="server" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Fecha Final:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tFF" runat="server" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bCrearC" runat="server" Text="Crear x Categoría" OnClick="bCrearC_Click" />
            </td>
            <td>
                <asp:Button ID="bCrearP" runat="server" Text="Crear x producto" OnClick="bCrearP_Click" />
            </td>
            <td>
                <asp:Button ID="bSalir1" runat="server" Text="Salir" OnClick="bSalir1_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    </asp:Content>
