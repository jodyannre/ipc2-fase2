﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="reportes.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.reportes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    Ventas por cliente
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="NIT del Cliente:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dCliente" runat="server" OnSelectedIndexChanged="dCliente_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Fecha inicial:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tFI" runat="server" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Fecha final:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tFF" runat="server" TextMode="Date"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bGenerar" runat="server" Text="Generar" OnClick="bGenerar_Click" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
<%--    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>

    <asp:GridView ID="GridView2" runat="server">
    </asp:GridView>

    <asp:GridView ID="GridView3" runat="server">
    </asp:GridView>

    <asp:GridView ID="GridView4" runat="server">
    </asp:GridView>--%>
</asp:Content>
