﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="crearMeta.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.crearMeta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h2>Asignar meta</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Empleado:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tEmpleado" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Cantidad"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCantidad" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Fecha inicial:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tInicio" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Fecha final:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tFinal" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Categoría:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCategoria" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="tCrear" runat="server" Text="Crear" OnClick="tCrear_Click" />
            </td>
            <td>
                <asp:Button ID="tSalir" runat="server" Text="Salir" OnClick="tSalir_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
