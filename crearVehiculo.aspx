﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="crearVehiculo.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.crearVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h2>Crear Vehículo</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Placa:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tPlaca" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="No_Motor:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tMotor" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="No_Chasis:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tChasis" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Estilo:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tEstilo" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Modelo:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tModelo" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Empleado"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tEmpleado" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Crear" OnClick="Button1_Click1" />
            </td>
            <td>
                <asp:Button ID="Button2" runat="server" Text="Salir" OnClick="Button2_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
