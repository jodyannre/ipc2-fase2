﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="pagar.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.pagar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h2>Pagar orden</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Cliente:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCliente" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="bCargar" runat="server" Text="Cargar" OnClick="bCargar_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Orden:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tOrden" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lResultado" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Cantidad:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCantidad" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lCalcular" runat="server">Equivalen a: </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Forma de pago:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tForma" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Crédito:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCredito" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Débito"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tDebito" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Cheque:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tCheque" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lMoneda" runat="server" Text="Moneda"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="dMoneda" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bCalcular" runat="server" Text="Calcular $" OnClick="bCalcular_Click" />
            </td>
            <td>
                <asp:Button ID="bPagar" runat="server" Text="Pagar" OnClick="Button1_Click" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" OnClick="bSalir_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
