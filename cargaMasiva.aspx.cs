﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Configuration;
using iText.StyledXmlParser.Jsoup.Nodes;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class cargaMasiva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bCargar_Click(object sender, EventArgs e)
        {
            if (fCargar.HasFile)
            {
                cargarXML();
            }
            
            
        }
        
        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("vAdmin");
        }


        protected void cargarXML()
        {
            string texto = File.ReadAllText("C:/Users/Jers_/Desktop/"+fCargar.FileName);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(texto);
            XmlNodeList categoria = xmlDoc.SelectNodes("definicion/categoria");
            XmlNodeList lista = xmlDoc.SelectNodes("definicion/lista");
            XmlNodeList meta = xmlDoc.SelectNodes("definicion/meta");
            XmlNodeList empleado = xmlDoc.SelectNodes("definicion/empleado");
            XmlNodeList moneda = xmlDoc.SelectNodes("definicion/moneda");
            XmlNodeList puesto = xmlDoc.SelectNodes("definicion/puesto");
            XmlNodeList lstXCliente = xmlDoc.SelectNodes("definicion/lstXCliente");
            XmlNodeList cliente = xmlDoc.SelectNodes("definicion/cliente");
            XmlNodeList depto = xmlDoc.SelectNodes("definicion/depto");
            XmlNodeList ciudad = xmlDoc.SelectNodes("definicion/ciudad");
            XmlNodeList producto = xmlDoc.SelectNodes("definicion/producto");

            string crearCategoria, crearLista, crearMeta, crearEmpleado, crearMoneda, crearPuesto, crearlstXcliente, crearCliente, crearDepto, crearCiudad;
            string crearProducto, crearDetalle;

            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            SqlConnection conexion = new SqlConnection(conexionSTR);
            conexion.Open();

            foreach (XmlNode nCategoria in categoria)
            {
                XmlNode codigo = nCategoria.SelectSingleNode("codigo");
                XmlNode nombre = nCategoria.SelectSingleNode("nombre");
                crearCategoria = "insert into Categoria_Producto values (" + codigo.InnerText.Trim() + ",'" + nombre.InnerText.Trim() + "')";
                realizarConsulta(crearCategoria, conexion);
            }
            foreach (XmlNode nProducto in producto)
            {
                XmlNode codigo = nProducto.SelectSingleNode("codigo");
                XmlNode nombre = nProducto.SelectSingleNode("nombre");
                XmlNode descripcion = nProducto.SelectSingleNode("descripcion");
                XmlNode categoriaP = nProducto.SelectSingleNode("categoria");
                crearProducto = "insert into Producto (Codigo,Nombre,Descripcion,Categoria) values (" + codigo.InnerText.Trim() + ",'" + nombre.InnerText.Trim() + "'," +
                    "'" + descripcion.InnerText.Trim() + "'," + categoriaP.InnerText.Trim() + ")";
                realizarConsulta(crearProducto, conexion);
            }
            foreach (XmlNode nLista in lista)
            {
                XmlNode codigo = nLista.SelectSingleNode("codigo");
                XmlNode nombre = nLista.SelectSingleNode("nombre");
                XmlNode vigencia_inicio = nLista.SelectSingleNode("vigencia_inicio");
                XmlNode vigencia_final = nLista.SelectSingleNode("vigencia_final");
                XmlNodeList detalle = xmlDoc.SelectNodes("definicion/lista[codigo='" + codigo.InnerText + "']/detalle/*");
                crearLista = "insert into Precio(Id,Nombre,Fecha_Inicio,Fecha_Final) values ("+codigo.InnerText.Trim()+",'"+nombre.InnerText.Trim() + "','"+vigencia_inicio.InnerText.Trim() + "'" +
                    ",'"+vigencia_final.InnerText.Trim() + "')";
                realizarConsulta(crearLista, conexion);
                foreach (XmlNode nDetalle in detalle)
                {
                    XmlNode codigo_producto = nDetalle.SelectSingleNode("codigo_producto");
                    XmlNode valor = nDetalle.SelectSingleNode("valor");
                    crearDetalle = "insert into Catalogo_Producto_Precio values(" + codigo_producto.InnerText.Trim() + "," + valor.InnerText.Trim() + "," + codigo.InnerText.Trim() + ")";
                    realizarConsulta(crearDetalle, conexion);
                }
            }
            foreach (XmlNode nDepto in depto)
            {
                XmlNode codigo = nDepto.SelectSingleNode("codigo");
                XmlNode nombre = nDepto.SelectSingleNode("nombre");
                crearDepto = "insert into Departamento values("+codigo.InnerText.Trim() + ",'"+nombre.InnerText.Trim() + "')";
                realizarConsulta(crearDepto, conexion);
            }
            foreach (XmlNode nCiudad in ciudad)
            {
                XmlNode codigo = nCiudad.SelectSingleNode("codigo");
                XmlNode nombre = nCiudad.SelectSingleNode("nombre");
                XmlNode codigo_depto = nCiudad.SelectSingleNode("codigo_depto");
                crearCiudad = "insert into Ciudad values(" + codigo.InnerText.Trim() + ",'" + nombre.InnerText.Trim() + "'," + codigo_depto.InnerText.Trim() + ")";
                realizarConsulta(crearCiudad, conexion);
            }
            foreach (XmlNode nCliente in cliente)
            {
                XmlNode NIT = nCliente.SelectSingleNode("NIT");
                XmlNode nombres = nCliente.SelectSingleNode("nombres");
                XmlNode apellidos = nCliente.SelectSingleNode("apellidos");
                XmlNode nacimiento = nCliente.SelectSingleNode("nacimiento");
                XmlNode direccion = nCliente.SelectSingleNode("direccion");
                XmlNode telefono = nCliente.SelectSingleNode("telefono");
                XmlNode celular = nCliente.SelectSingleNode("celular");
                XmlNode email = nCliente.SelectSingleNode("email");
                XmlNode ciudadC = nCliente.SelectSingleNode("ciudad");
                XmlNode deptoC = nCliente.SelectSingleNode("depto");
                XmlNode limite_credito = nCliente.SelectSingleNode("limite_credito");
                XmlNode dias_credito = nCliente.SelectSingleNode("dias_credito");

                crearCliente = "insert into Cliente values('" + NIT.InnerText.Trim() + "','" + nombres.InnerText.Trim() + "','" + apellidos.InnerText.Trim() + "'," +
                    "'" + nacimiento.InnerText.Trim() + "','" + direccion.InnerText.Trim() + "','" + telefono.InnerText.Trim() + "','" + celular.InnerText.Trim() + "','" + email.InnerText.Trim() + "'," + ciudadC.InnerText.Trim() + "" +
                    "," + limite_credito.InnerText.Trim() + "," + limite_credito.InnerText.Trim()+","+ dias_credito.InnerText.Trim() + ")";
                realizarConsulta(crearCliente, conexion);

            }
            foreach (XmlNode nlst in lstXCliente)
            {
                XmlNode codigo_lista = nlst.SelectSingleNode("codigo_lista");
                XmlNode clienteC = nlst.SelectSingleNode("cliente");
                crearlstXcliente = "insert into Catalogo_Precios values ("+codigo_lista.InnerText.Trim() + ","+clienteC.InnerText.Trim() + ")";
                realizarConsulta(crearlstXcliente, conexion);
            }
            foreach (XmlNode nPuesto in puesto)
            {
                XmlNode codigo = nPuesto.SelectSingleNode("codigo");
                XmlNode nombre = nPuesto.SelectSingleNode("nombre");
                crearPuesto = "insert into Puesto values (" + codigo.InnerText.Trim() + ",'" + nombre.InnerText.Trim() + "')";
                realizarConsulta(crearPuesto, conexion);
            }
            foreach (XmlNode nEmpleado in empleado)
            {
                XmlNode NIT = nEmpleado.SelectSingleNode("NIT");
                XmlNode nombres = nEmpleado.SelectSingleNode("nombres");
                XmlNode apellidos = nEmpleado.SelectSingleNode("apellidos");
                XmlNode nacimiento = nEmpleado.SelectSingleNode("nacimiento");
                XmlNode direccion = nEmpleado.SelectSingleNode("direccion");
                XmlNode telefono = nEmpleado.SelectSingleNode("telefono");
                XmlNode celular = nEmpleado.SelectSingleNode("celular");
                XmlNode email = nEmpleado.SelectSingleNode("email");
                XmlNode codigo_puesto = nEmpleado.SelectSingleNode("codigo_puesto");
                XmlNode codigo_jefe = nEmpleado.SelectSingleNode("codigo_jefe");
                XmlNode pass = nEmpleado.SelectSingleNode("pass");

                if (codigo_jefe.InnerText != "")
                {
                    crearEmpleado = "insert into Empleado (NIT,Nombre,Apellido,Nacimiento,Direccion,Telefono,Celular,Email,Puesto,Jefe, Contraseña" +
                        ") values ('" + NIT.InnerText.Trim() + "','" + nombres.InnerText.Trim() + "','" + apellidos.InnerText.Trim() + "'," +
                        "'" + nacimiento.InnerText.Trim() + "','" + direccion.InnerText.Trim() + "','" + telefono.InnerText.Trim() + "','" + celular.InnerText.Trim() + "','" + email.InnerText.Trim() + "'," + codigo_puesto.InnerText.Trim() + "" +
                        ",'" + codigo_jefe.InnerText.Trim() + "','" + pass.InnerText.Trim() + "')";
                    realizarConsulta(crearEmpleado, conexion);
                }
                else
                {
                    crearEmpleado = "insert into Empleado (NIT,Nombre,Apellido,Nacimiento,Direccion,Telefono,Celular,Email,Puesto,Contraseña" +
                        ")values ('" + NIT.InnerText.Trim() + "','" + nombres.InnerText.Trim() + "','" + apellidos.InnerText.Trim() + "'," +
                        "'" + nacimiento.InnerText.Trim() + "','" + direccion.InnerText.Trim() + "','" + telefono.InnerText.Trim() + "','" + celular.InnerText.Trim() + "','" + email.InnerText.Trim() + "'," + codigo_puesto.InnerText.Trim() + "" +
                        ",'" +pass.InnerText.Trim() + "')";
                    realizarConsulta(crearEmpleado, conexion);
                }

            }
            int contador = 1;
            foreach (XmlNode nMeta in meta)
            {
                XmlNode NIT_empleado = nMeta.SelectSingleNode("NIT_empleado");
                XmlNode mes_meta = nMeta.SelectSingleNode("mes_meta");
                XmlNodeList detalle = xmlDoc.SelectNodes("definicion/meta[NIT_empleado='" + NIT_empleado.InnerText + "']/detalle/*");
                crearMeta = "insert into Meta values ("+contador+",'" + NIT_empleado.InnerText.Trim() + "','01/" + mes_meta.InnerText.Trim() + "')";
                realizarConsulta(crearMeta, conexion);
                foreach (XmlNode nDetalle in detalle)
                {
                    XmlNode codigo_producto = nDetalle.SelectSingleNode("codigo_producto");
                    XmlNode meta_venta = nDetalle.SelectSingleNode("meta_venta");
                    crearDetalle = "insert into Catalogo_Producto_Meta values (" + contador + "," + codigo_producto.InnerText.Trim() + "," +
                        "" + meta_venta.InnerText.Trim() + ")";
                    
                    realizarConsulta(crearDetalle, conexion);
                    //Response.Write("<script languaje=javascript> alert(' Valor: " + meta_venta.InnerText + "');</script>");
                }
                contador++;
            }
            foreach (XmlNode nMoneda in moneda)
            {
                XmlNode nombre = nMoneda.SelectSingleNode("nombre");
                XmlNode simbolo = nMoneda.SelectSingleNode("simbolo");
                XmlNode tasa = nMoneda.SelectSingleNode("tasa");
                crearMoneda = "insert into Moneda values ('" + nombre.InnerText.Trim() + "','" + simbolo.InnerText.Trim() + "'," + tasa.InnerText.Trim() + ")";
                realizarConsulta(crearMoneda, conexion);
            }
            conexion.Close();
        }

        protected void realizarConsulta(string query, SqlConnection conexion)
        {
            SqlCommand cmd = new SqlCommand(query, conexion);
            cmd.ExecuteNonQuery();
        }
    }
}