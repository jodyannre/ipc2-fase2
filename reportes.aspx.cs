﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class reportes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "select * from Cliente";
            string nombre = "NIT";
            string valor = "NIT";
            cargarList(query, nombre, valor, dCliente);
        }

        protected void bGenerar_Click(object sender, EventArgs e)
        {
            generarReporte();
            
        }
        protected void generarReporte()
        {
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "Reporte1.pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph titulo = new Paragraph();
            DateTime fechaI = Convert.ToDateTime(tFI.Text);
            string dateI = fechaI.ToString("dd/MM/yyyy");
            DateTime fechaF = Convert.ToDateTime(tFF.Text);
            string dateF = fechaF.ToString("dd/MM/yyyy");
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);


            string query1 = "Select NIT, Nombre, Direccion from Cliente where NIT = " + dCliente.SelectedValue.ToString();
            string query2 = "Select NIT, Empleado.Nombre, Puesto.Nombre as Puesto from Empleado inner join Puesto " +
                "on Puesto.Id_Puesto = Empleado.Puesto inner join Orden on Orden.Empleado_NIT = Empleado.NIT where Empleado.NIT in " +
                "(select Empleado_NIT from Orden where Cliente_NIT = '"+ dCliente.SelectedValue.ToString() + "') and " +
                "Orden.Fecha_Limite <= '"+dateF+"' and Orden.Fecha_Limite >= '"+dateI+"' group by NIT, Empleado.Nombre, Puesto.Nombre";


            string query3 = "select Producto.Nombre as Producto, Catalogo_Producto_Precio.Precio, sum(Productos_Orden.Cantidad) as Cantidad, " +
                "sum(Productos_Orden.Total) as total from Producto inner join Catalogo_Producto_Precio on Producto.Codigo = " +
                "Catalogo_Producto_Precio.Producto inner join Productos_Orden on Productos_Orden.Producto = Catalogo_Producto_Precio.Id inner " +
                "join Orden on Orden.Id = Productos_Orden.Orden where Orden.Cliente_NIT = '"+ dCliente.SelectedValue.ToString() + "' " +
                "and Orden.Fecha_Limite <= '"+dateF+"' and " +
                "Orden.Fecha_Limite >= '"+dateI+"' group by Producto.Nombre, Catalogo_Producto_Precio.Precio";



            string query4 = "select sum(Total) as Sumatoria from Orden where Orden.Cliente_NIT = "+ dCliente.SelectedValue.ToString() + " " +
                "and Orden.Fecha_Limite <= '"+dateF+"' and Orden.Fecha_Limite >= '"+dateI+"'";

            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_CENTER;
            DataTable dtCliente = consultasPDF(query1);
            DataTable dEmpleado = consultasPDF(query2);
            DataTable dDetalle = consultasPDF(query3);
            DataTable dTotal = consultasPDF(query4);
            PdfPTable tCliente = new PdfPTable(3);
            PdfPTable tEmpleado = new PdfPTable(3);
            PdfPTable tDetalle = new PdfPTable(4);
            PdfPTable tTotal = new PdfPTable(1);

            tCliente.AddCell("NIT");
            tCliente.AddCell("Nombre");
            tCliente.AddCell("Dirección");

            foreach (DataRow fila in dtCliente.Rows)
            {
                tCliente.AddCell(fila["NIT"].ToString());
                tCliente.AddCell(fila["Nombre"].ToString());
                tCliente.AddCell(fila["Direccion"].ToString());
            }

            tEmpleado.AddCell("NIT");
            tEmpleado.AddCell("Nombre");
            tEmpleado.AddCell("Puesto");

            foreach (DataRow fila in dEmpleado.Rows)
            {
                tEmpleado.AddCell(fila["NIT"].ToString());
                tEmpleado.AddCell(fila["Nombre"].ToString());
                tEmpleado.AddCell(fila["Puesto"].ToString());
            }

            tDetalle.AddCell("Producto");
            tDetalle.AddCell("Precio");
            tDetalle.AddCell("Cantidad");
            tDetalle.AddCell("Total");

            foreach (DataRow fila in dDetalle.Rows)
            {
                tDetalle.AddCell(fila["Producto"].ToString());
                tDetalle.AddCell(fila["Precio"].ToString());
                tDetalle.AddCell(fila["Cantidad"].ToString());
                tDetalle.AddCell(fila["Total"].ToString());
            }

            tTotal.AddCell("Sumatoria");
            foreach (DataRow fila in dTotal.Rows)
            {
                tTotal.AddCell(fila["Sumatoria"].ToString());
            }
            parrafo.Add("Ventas por cliente");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Cliente");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tCliente);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Empleado");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tEmpleado);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Detalle de venta");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tDetalle);
            doc.Add(Chunk.NEWLINE);
            parrafo.Add("Sumatoria");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(tTotal);
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);

            parrafo.Add("Detalle Venta");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);


            doc.Close();



        }

        protected DataTable consultasPDF(string query)
        {
            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                adapter.Fill(tabla);
                return tabla;
            }

        }
        protected void cargarList(String query, string nombre, string valor, DropDownList lista)
        {

            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;


            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                SqlCommand comando = new SqlCommand(query, conexion);
                conexion.Open();
                lista.DataSource = comando.ExecuteReader();
                lista.DataTextField = nombre;
                lista.DataValueField = valor;
                lista.DataBind();
                conexion.Close();
            }
        }

        protected void dCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}