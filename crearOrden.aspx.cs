﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class crearOrden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string valor = Application["nitEmpleado"].ToString();
            tEmpleado.Text = valor;
            tOrden.Text = (Session["idOrden"]).ToString();
            string query = "select * from Cliente order by Cliente.Nombre";
            string nombre = "NIT";
            string val = "NIT";
            if (!this.IsPostBack)
            {
                cargarList(query, nombre, val, dCliente);

                query = "select * from Precio where Estado = 1";
                nombre = "Nombre";
                val = "Id";
                cargarList(query, nombre, val, dLista);
            }
        }

        protected void bVerLista_Click(object sender, EventArgs e)
        {
            this.Session["verLista"] = Convert.ToInt32(dListaA.SelectedValue);
            Response.Write("<script> window.open('verListaProductos.aspx');</script>");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "agregarProductoOrden";
            int producto = Convert.ToInt32(dProducto.SelectedValue);
            int cantidad = Convert.ToInt32(tCantidad.Text);
            comando.Parameters.AddWithValue("@producto", producto);
            comando.Parameters.AddWithValue("@cantidad", cantidad);
            comando.Parameters.AddWithValue("@orden", Session["idOrden"]);
            comando.Parameters.AddWithValue("@lista", Convert.ToInt32(dListaA.SelectedValue)); //agregado en v2
            int orden = Convert.ToInt32(Session["idOrden"]);
            comando.ExecuteNonQuery();
            
            tCantidad.Text = string.Empty;

            conexion.Close();
            conexion.Open();

            SqlCommand comando2 = new SqlCommand();
            comando2.Connection = conexion;
            comando2.CommandType = CommandType.StoredProcedure;
            comando2.CommandText = "actualizarTotalOrden";
            
            comando2.Parameters.AddWithValue("@orden", orden);
            comando2.Parameters.AddWithValue("@lista", Convert.ToInt32(dListaA.SelectedValue));
            var subtotal = new SqlParameter
            {
                ParameterName = "@subtotal",
                Direction = ParameterDirection.Output,
                Size = 60000
            };
            subtotal.SqlDbType = SqlDbType.Decimal;
            subtotal.Precision = 18;
            subtotal.Scale = 2;
            comando2.Parameters.Add(subtotal);
            comando2.ExecuteNonQuery();
            decimal salida = Convert.ToDecimal(comando2.Parameters["@subtotal"].Value);
            tSubtotal.Text = salida.ToString("C",new CultureInfo("en-US"));
            conexion.Close();
        }

        protected void bEliminar_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "quitarProductoOrden";
            int producto = Convert.ToInt32(dProducto.SelectedValue);
            comando.Parameters.AddWithValue("@producto", producto);
            comando.Parameters.AddWithValue("@orden", Session["idOrden"]);
            comando.Parameters.AddWithValue("@cantidad", Convert.ToInt32(tCantidad.Text));
            comando.Parameters.AddWithValue("@lista", Convert.ToInt32(dListaA.SelectedValue)); //agregado a v2
            int orden = Convert.ToInt32(Session["idOrden"]);
            comando.ExecuteNonQuery();

            tCantidad.Text = string.Empty;

            conexion.Close();
            conexion.Open();

            SqlCommand comando2 = new SqlCommand();
            comando2.Connection = conexion;
            comando2.CommandType = CommandType.StoredProcedure;
            comando2.CommandText = "actualizarTotalOrden";

            comando2.Parameters.AddWithValue("@orden", orden);
            comando2.Parameters.AddWithValue("@lista", Convert.ToInt32(dListaA.SelectedValue));
            var subtotal = new SqlParameter
            {
                ParameterName = "@subtotal",
                Direction = ParameterDirection.Output,
                Size = 60000
            };
            comando2.Parameters.Add(subtotal);
            comando2.ExecuteNonQuery();
            decimal salida = Convert.ToDecimal(comando2.Parameters["@subtotal"].Value);
            tSubtotal.Text = salida.ToString();
            conexion.Close();
        }

        protected void bCrear_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "agregarOrden";
            string cliente = dCliente.SelectedValue;
            string empleado = tEmpleado.Text;
            DateTime fecha = DateTime.Today;
            string date = fecha.ToString("dd/MM/yyyy");
            int orden = Convert.ToInt32(Session["idOrden"]);
            comando.Parameters.AddWithValue("@id", orden);
            comando.Parameters.AddWithValue("@cliente", cliente);
            comando.Parameters.AddWithValue("@empleado", empleado);
            comando.Parameters.AddWithValue("@fecha", date);

            comando.ExecuteNonQuery();

            tCantidad.Text = string.Empty;

            conexion.Close();

            salir();
        }

        protected void bDatos_Click(object sender, EventArgs e)
        {
            Response.Write("<script> window.open('verDatosCliente.aspx');</script>");
        }

        protected void bCargarCliente_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            string query = "select Disponible from Cliente where NIT ='" + dCliente.SelectedValue + "';";
            conexion.Open();
            SqlCommand comando = new SqlCommand(query,conexion);
            decimal disponible = Convert.ToDecimal(comando.ExecuteScalar());
            tCredito.Text = disponible.ToString("C", new CultureInfo("en-US"));
            string cliente = dCliente.SelectedValue;
            query = "select Precio from Catalogo_Precios inner join Cliente on Catalogo_Precios.Cliente = Cliente.NIT " +
                "inner join Precio on Catalogo_Precios.Precio = Precio.Id " +
                "where Cliente.NIT = '"+cliente+ "' and Precio.Estado = 1; ";
            comando = new SqlCommand(query, conexion);
            try
            {
                query = "select Precio.Id from Catalogo_Precios inner join Cliente on Cliente.NIT = Catalogo_Precios.Cliente" +
                    " inner join Precio on Catalogo_Precios.Precio = Precio.Id where Precio.Estado = 1 and " +
                    "Cliente.NIT = '"+cliente+"'";
                string nombre = "Id";
                string val = "Id";
                cargarList(query, nombre, val, dListaA);
   
            }
            catch(Exception ex)
            {

            }
            conexion.Close();
        }
        protected void cargarList(String query, string nombre, string valor, DropDownList lista)
        {

            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            
            
                using (SqlConnection conexion = new SqlConnection(conexionSTR))
                {
                    SqlCommand comando = new SqlCommand(query, conexion);
                    conexion.Open();
                    lista.DataSource = comando.ExecuteReader();
                    lista.DataTextField = nombre;
                    lista.DataValueField = valor;
                    lista.DataBind();
                    conexion.Close();
                }
        }

        protected void bAsignar_Click(object sender, EventArgs e)
        {

            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "asignarListaCliente";
            comando.Parameters.AddWithValue("@listaAsignar", Convert.ToInt32(dLista.SelectedValue));
            comando.Parameters.AddWithValue("@cliente", Convert.ToInt32(dCliente.SelectedValue));


            var resultado = new SqlParameter
            {
                ParameterName = "@resultado",
                Direction = ParameterDirection.Output,
                Size = 10
            };
            comando.Parameters.Add(resultado);
            comando.ExecuteNonQuery();
            int salida = Convert.ToInt32(comando.Parameters["@resultado"].Value);

            conexion.Close();
            string mensaje = "";
            switch (salida)
            {
                case 0:
                    mensaje = "Ya posee una lista activa en eses rango de fechas.";
                    break;
                case 1:
                    mensaje = "Lista asignada.";
                    break;
                default:
                    break;
            }
            Response.Write("<script languaje=javascript> alert('" + mensaje + "');</script>");
        }

        protected void bSalir_Click(object sender, EventArgs e)
        {

            salir();
            
        }

        protected void bmProducto_Click(object sender, EventArgs e)
        {
            string query = "select Producto.Nombre,Producto.Codigo from Producto inner join Catalogo_Producto_Precio " +
            "on Producto.Codigo = Catalogo_Producto_Precio.Producto " +
            "inner join Precio on Catalogo_Producto_Precio.Lista = Precio.Id where Precio.Estado = 1 and " +
            "Precio.Id = "+ Convert.ToInt32(dListaA.SelectedValue);
            string nombre = "Nombre";
            string val = "Codigo";
            cargarList(query, nombre, val, dProducto);
        }
        protected void salir()
        {
            string query = "select Puesto.Id_Puesto from Puesto inner join Empleado on Empleado.Puesto = " +
                "Puesto.Id_Puesto where Empleado.NIT = " + Application["nitEmpleado"].ToString();
            int puesto = 0;

            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                puesto = Convert.ToInt32(comando.ExecuteScalar());
            }

            switch (puesto)
            {
                case 1:
                    Response.Redirect("vVendedor");
                    break;
                case 2:
                    Response.Redirect("vJefes");
                    break;
                case 3:
                    Response.Redirect("vJefes");
                    break;
                default:
                    break;
            }
        }

        protected void crearId()
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "crearOrden";

            var orden = new SqlParameter
            {
                ParameterName = "@id",
                Direction = ParameterDirection.Output,
                Size = 1000
            };
            comando.Parameters.Add(orden);
            comando.ExecuteNonQuery();
            Session["idOrden"] = Convert.ToInt32(comando.Parameters["@id"].Value);
            conexion.Close();
            //Response.Redirect("crearOrden.aspx");
        }
    }
}