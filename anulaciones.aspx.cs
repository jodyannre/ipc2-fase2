﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class anulaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            string consulta = ("select Empleado.Nombre as Empleado, NIT, Orden.Id as Orden, Estado_Orden.Nombre as Estado, " +
                "Orden.Abonado as Abonado, Orden.Total as Total from Empleado inner join Orden " +
                "on Orden.Empleado_NIT = Empleado.NIT inner join Estado_Orden on Orden.Estado_Orden = Estado_Orden.Id " +
                "where Orden.Estado_Orden = 1 and Jefe = ") + Application["nitEmpleado"].ToString() + "or Orden.Empleado_NIT = " +
                "" + Application["nitEmpleado"].ToString() + " and Orden.Estado_Orden = 1";
            DataTable tabla = new DataTable();
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlDataAdapter adaptador = new SqlDataAdapter(consulta, conexion);
                adaptador.Fill(tabla);
                conexion.Close();
            }
            gAnulacion.DataSource = tabla;
            gAnulacion.DataBind();
        }

        protected void bCancelar_Click(object sender, EventArgs e)
        {

            //string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;

            //SqlConnection conexion = new SqlConnection(conexionSTR);
            //conexion.Open();
            //SqlCommand comando = new SqlCommand();
            //comando.Connection = conexion;
            //comando.CommandType = CommandType.StoredProcedure;
            //comando.CommandText = "anularOrden";
            //comando.Parameters.AddWithValue("@orden", Convert.ToInt32(tOrden.Text));
            //var respuesta = new SqlParameter
            //{
            //    ParameterName = "@respuesta",
            //    Direction = ParameterDirection.Output,
            //    Size = 10
            //};
            //comando.Parameters.Add(respuesta);
            //comando.ExecuteNonQuery();
            //int salida = Convert.ToInt32(comando.Parameters["@respuesta"].Value);
            //string mensaje = "";
            //switch (salida)
            //{
            //    case 0:
            //        mensaje = "Orden inexistente o no válida";
            //        break;
            //    case 1:
            //        mensaje = "Orden anulada.";
            //        break;
            //}
            //Response.Write("<script languaje=javascript> alert('" + mensaje + "');</script>");
            generarPDF();

        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("vJefes");
        }

        protected void generarPDF()
        {

            int idAnulacion = generarAnulacion();
            System.IO.FileStream fs = new FileStream(Server.MapPath("") + "\\" + "anulacion-" + idAnulacion + ".pdf", FileMode.Create);
            Document doc = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            Paragraph parrafo = new Paragraph();
            parrafo.Alignment = Element.ALIGN_CENTER;

            string query1 = "select Anulacion.Id as Anulacion, convert(varchar,Anulacion.Fecha,3) as Fecha, Orden.Id as Orden, " +
                "Orden.Abonado as Facturado, Orden.Total, Orden.Cliente_NIT as Cliente, Orden.Empleado_NIT as Vendedor from Anulacion inner " +
                "join Orden on Orden.Anulacion = Anulacion.Id where Orden.Anulacion = " + idAnulacion;

            DataTable dAnulacion = consultasPDF(query1);
            PdfPTable tAnulacion = new PdfPTable(7);

            tAnulacion.AddCell("Anulación");
            tAnulacion.AddCell("Fecha");
            tAnulacion.AddCell("Orden");
            tAnulacion.AddCell("Facturado");
            tAnulacion.AddCell("Total");
            tAnulacion.AddCell("Cliente");
            tAnulacion.AddCell("Vendedor");
            foreach (DataRow fila in dAnulacion.Rows)
            {
                tAnulacion.AddCell(fila["Anulacion"].ToString());
                tAnulacion.AddCell(fila["Fecha"].ToString());
                tAnulacion.AddCell(fila["Orden"].ToString());
                tAnulacion.AddCell(fila["Facturado"].ToString());
                tAnulacion.AddCell(fila["Total"].ToString());
                tAnulacion.AddCell(fila["Cliente"].ToString());
                tAnulacion.AddCell(fila["Vendedor"].ToString());
            }
            parrafo.Add("Nota de crédito por anulación");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tAnulacion);
            doc.Close();

            int idFactura = generarFactura();
            fs = new FileStream(Server.MapPath("") + "\\" + "factura-" + idFactura + ".pdf", FileMode.Create);
            doc = new Document(PageSize.A4, 25, 25, 30, 30);
            writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            query1 = "select Id_Factura as Factura, Serie, Orden.Cliente_NIT as Cliente, Orden.Empleado_NIT as Vendedor, " +
                "Convert(varchar, Fecha, 3) as Fecha, Orden, Abonado as Total, Orden.Anulacion as Anulacion from Factura " +
                "inner join Orden on Orden.Id = Factura.Orden where Factura.Id_Factura = "+idFactura;


            DataTable dFactura = consultasPDF(query1);
            PdfPTable tFactura = new PdfPTable(7);

            tFactura.AddCell("Factura");
            tFactura.AddCell("Serie");
            tFactura.AddCell("Cliente");
            tFactura.AddCell("Vendedor");
            tFactura.AddCell("Fecha");
            tFactura.AddCell("Orden");
            tFactura.AddCell("Total");

            foreach (DataRow fila in dFactura.Rows)
            {
                tFactura.AddCell(fila["Factura"].ToString());
                tFactura.AddCell(fila["Serie"].ToString());
                tFactura.AddCell(fila["Cliente"].ToString());
                tFactura.AddCell(fila["Vendedor"].ToString());
                tFactura.AddCell(fila["Fecha"].ToString());
                tFactura.AddCell(fila["Orden"].ToString());
                tFactura.AddCell(fila["Total"].ToString());
            }
            parrafo.Add("Factura por anulación");
            doc.Add(parrafo);
            parrafo.Clear();
            doc.Add(Chunk.NEWLINE);
            doc.Add(Chunk.NEWLINE);
            doc.Add(tFactura);
            doc.Close();



        }
        protected int generarFactura()
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            DateTime date = DateTime.Today;
            string fecha = date.ToString("dd/MM/yyyy");
            string query = "insert into Factura values ('AC','"+fecha+"',"+Convert.ToInt32(tOrden.Text)+")";
            string query2 = "select MAX(Id_Factura) AS Id from Factura";
            string query3 = "update Orden set Estado_Orden = 6 where Id = " + Convert.ToInt32(tOrden.Text);
            int idFactura = 0;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))

            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                comando.ExecuteNonQuery();
                comando = new SqlCommand(query2, conexion);
                idFactura = Convert.ToInt32(comando.ExecuteScalar());
                comando = new SqlCommand(query3, conexion);
                comando.ExecuteNonQuery();

            }
            return idFactura;

        }

        protected DataTable consultasPDF(string query)
        {
            DataTable tabla = new DataTable();
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query,conexion);
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                adapter.Fill(tabla);
                return tabla;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            generarFactura();
        }

        protected int generarAnulacion()
        {
            string conexionSTR = ConfigurationManager.ConnectionStrings["conexionBD"].ConnectionString;
            DateTime date = DateTime.Today;
            int idAnulacion = 0;
            string fecha = date.ToString("dd/MM/yyyy");
            string query = "insert into Anulacion values ('" + fecha + "')";
            string query2 = "select MAX(id) AS Id from Anulacion";
            
            
            using (SqlConnection conexion = new SqlConnection(conexionSTR))
                
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand(query, conexion);
                comando.ExecuteNonQuery();
                comando = new SqlCommand(query2, conexion);
                idAnulacion = Convert.ToInt32(comando.ExecuteScalar());
                string agregarAnulacion = "update Orden set Anulacion = " + idAnulacion + " where Id = " + Convert.ToInt32(tOrden.Text);
                comando = new SqlCommand(agregarAnulacion, conexion);
                comando.ExecuteNonQuery();               

            }
            return idAnulacion;
        }
    }
}