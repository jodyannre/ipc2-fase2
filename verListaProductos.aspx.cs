﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class verListaProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            string consulta = ("select Codigo, Producto.Nombre, Catalogo_Producto_Precio.Precio from Producto inner join Catalogo_Producto_Precio " +
                "on Producto.Codigo = Catalogo_Producto_Precio.Producto inner join Precio on Precio.Id = Catalogo_Producto_Precio.Lista where Precio.Estado = 1 " +
                "and Precio.Id = "+Session["verLista"]);
            DataTable tabla = new DataTable();
            using (SqlConnection conexion = new SqlConnection(conexionString))
            {
                conexion.Open();
                SqlDataAdapter adaptador = new SqlDataAdapter(consulta, conexion);
                adaptador.Fill(tabla);
                conexion.Close();
            }
            gLista.DataSource = tabla;
            gLista.DataBind();

        }
    }
}