﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Ipc2_Vacaciones_2020.Clases
{
    public class Empleado
    {
        protected string nit;
        protected string nombre;
        protected string apellido;
        protected DateTime fechaNacimiento;
        protected string dirDomicilio;
        protected string telDomicilio;
        protected int celular;
        protected string email;
        protected string contraseña;
    }
}