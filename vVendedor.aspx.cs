﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class vVendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            //string consulta = ("select * from Empleado;");
            //DataTable tabla = new DataTable();
            //using (SqlConnection conexion = new SqlConnection(conexionString))
            //{
            //    conexion.Open();
            //    SqlDataAdapter adaptador = new SqlDataAdapter(consulta, conexion);
            //    adaptador.Fill(tabla);
            //    conexion.Close();
           // }
            //gMeta.DataSource = tabla;
            //gMeta.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
        }

        protected void gMeta_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("login.aspx");
        }

        protected void bCrear_Click(object sender, EventArgs e)
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "crearOrden";

            var orden = new SqlParameter
            {
                ParameterName = "@id",
                Direction = ParameterDirection.Output,
                Size = 1000
            };
            comando.Parameters.Add(orden);
            comando.ExecuteNonQuery();
            Session["idOrden"] = Convert.ToInt32(comando.Parameters["@id"].Value);
            conexion.Close();
            Response.Redirect("crearOrden.aspx");
        }
    }
}