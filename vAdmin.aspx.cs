﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class vAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearCliente");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearEmpleado");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearVehiculo");
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearMeta");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearCategoriaProducto");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearProducto");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("crearVehiculo");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("cargaMasiva");
        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("login");
        }
    }
}