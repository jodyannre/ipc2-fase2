﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Proyecto_Ipc2_Vacaciones_2020
{
    public partial class vJefes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bOrden_Click(object sender, EventArgs e)
        {
            Response.Redirect("gestionOrden");
        }

        protected void bSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("login");
        }

        protected void bVenta_Click(object sender, EventArgs e)
        {
            crearId();
        }

        protected void bAnulacion_Click(object sender, EventArgs e)
        {
            Response.Redirect("anulaciones");
        }
        protected void crearId()
        {
            string conexionString = @"Data Source=(local);User ID=;Password=; Initial Catalog = fase2; Integrated Security=True;";
            SqlConnection conexion = new SqlConnection(conexionString);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "crearOrden";

            var orden = new SqlParameter
            {
                ParameterName = "@id",
                Direction = ParameterDirection.Output,
                Size = 1000
            };
            comando.Parameters.Add(orden);
            comando.ExecuteNonQuery();
            Session["idOrden"] = Convert.ToInt32(comando.Parameters["@id"].Value);
            conexion.Close();
            Response.Redirect("crearOrden.aspx");
        }

        protected void bReporte1_Click(object sender, EventArgs e)
        {
            Response.Redirect("rVentasxCliente.aspx");
        }

        protected void bReporte2_Click(object sender, EventArgs e)
        {
            Response.Redirect("rVentasxProducto.aspx");
        }
    }
}