﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h2>Login</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Usuario: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tUsuario" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Contraseña: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tPass" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bPagar" runat="server" Text="Pagar" OnClick="bPagar_Click" />
            </td>
            <td>
                <asp:Button ID="bIngresar" runat="server" Text="Ingresar" OnClick="Button1_Click" style="height: 35px" />
            </td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" />
            </td>
        </tr>
    </table>
</asp:Content>
