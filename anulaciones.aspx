﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="anulaciones.aspx.cs" Inherits="Proyecto_Ipc2_Vacaciones_2020.anulaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Panel ID="panel" runat="server" ScrollBars="Both" Height="300" Width="100%">
        <asp:GridView ID="gAnulacion" runat="server">
        </asp:GridView>
     </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <table style="width:100%;">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Orden:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tOrden" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="bAnular" runat="server" Text="Anular" OnClick="bCancelar_Click" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="bSalir" runat="server" Text="Salir" OnClick="bSalir_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
